new Vue({
  el: '#app',
  data: {
    myHealth: 100,
    monsterHealth: 100,
    isGameRunning: false,
    turns: []
  },
  methods: {
    startNewGame: function() {
      this.isGameRunning = true;
      this.myHealth = 100;
      this.monsterHealth = 100;
      this.turns = []; 
    },
    attack: function() {
      let damage = this.calculateDamage(3, 10);
      this.monsterHealth -= damage
      this.turns.unshift({
        isPlayer: true,
        text: 'Player hits monster for ' + damage
      });

      if (this.isEndGame()) {
        return;
      }
      
      this.monsterAttack();
    },
    specialAttack: function() {
      let damage = this.calculateDamage(10, 20);
      this.monsterHealth -= damage
      this.turns.unshift({
        isPlayer: true,
        text: 'Player hits monster with special attack for ' + damage
      });
      if (this.isEndGame()) {
        return;
      }
      
      this.monsterAttack();
    },
    heal: function() {
      if (this.myHealth <= 90) {
        this.myHealth += 10;
      } else {
        this.myHealth = 100;
      }
      
      this.turns.unshift({
        isPlayer: true,
        text: 'Player heals for 10'
      });
      this.monsterAttack();
     },
    giveUp: function() {
      this.isGameRunning = false;
    },
    monsterAttack: function () {
      let damage = this.calculateDamage(5, 12);
      this.myHealth -= damage;
      this.isEndGame();
      this.turns.unshift({
        isPlayer: false,
        text: 'Monster hits player for ' + damage
      });
    },
    calculateDamage: function(min, max) {
      return Math.max(Math.floor(Math.random() * max) + 1, min);
    },
    isEndGame: function() {
      if (this.myHealth <= 0) {
        this.myHealth = 0;
        this.isGameRunning = false;
        if (confirm('You lost! Play new game?')) {
          this.startNewGame();
        }
        return true;
      } else if (this.monsterHealth <= 0) {
        this.monsterHealth = 0;
        this.isGameRunning = false;
        if (confirm('You won! Play new game?')) {
          this.startNewGame();
        }
        return true;
      }

      return false;
    }
  }
});
